<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'namespace' => 'WEB',
], function () {
    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::get('/register', 'AuthController@register')->name('auth.register');
        Route::post('/register', 'AuthController@actionRegister')->name('auth.actionRegister');
        Route::get('/login', 'AuthController@login')->name('auth.login');
        Route::post('/login', 'AuthController@actionLogin')->name('auth.actionLogin');
        Route::post('/forget-password', 'AuthController@forgetPassword');
    });

    Route::middleware([])->group(function () {
        Route::group([
            'prefix' => 'user',
        ], function () {
            Route::post('/change-password', 'UserController@changePassword');
            Route::get('/show/{userId}', 'UserController@show');
        });
    });
});
