<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'API',
    'prefix' => 'v1',
], function () {
    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::post('/register', 'AuthController@register');
        Route::post('/login', 'AuthController@login');
        Route::post('/forget-password', 'AuthController@forgetPassword');
    });

    Route::middleware(['auth.jwt'])->group(function () {
        Route::group([
            'prefix' => 'user',
        ], function () {
            Route::post('/change-password', 'UserController@changePassword');
            Route::get('/show/{userId}', 'UserController@show');
        });
    });
});
