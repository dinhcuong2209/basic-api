<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|string|min:6|max:15',
            'new_password' => 'required|string|min:6|max:15|different:old_password',
            'confirm_password' => 'required|string|min:6|max:15|same:new_password',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Please enter :attribute',
        ];
    }
}
