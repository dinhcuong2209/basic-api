<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'full_name' => 'required|string',
            'email' => 'required|email|string|min:6|max:127|unique:users',
            'password' => 'required|string|min:6|max:15',
            'role' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Hãy nhập :attribute',
            'email' => 'Email không đúng định dạng'
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
            'password' => 'Password',
            'role' => 'Role'
        ];
    }
}
