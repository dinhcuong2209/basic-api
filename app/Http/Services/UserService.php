<?php
namespace App\Http\Services;

use App\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Auth;

class UserService extends BaseService {

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    private function getUserById($userId)
    {
        return $this->user->whereId($userId);
    }

    private function getUserByEmail($email)
    {
        return $this->user->whereEmail($email);
    }

    public function store($data)
    {
        $save = $this->user->create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role' => $data['role'],
        ]);
        if (!$save) {
            return $this->error('Có lỗi trong quá trình đăng ký', self::BAD_REQUEST);
        }
        return $this->success('Đăng ký thành công', self::CREATED);
    }

    public function login($credentials)
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->error('Sai email hoặc mật khẩu', self::UNAUTHORIZED);
        }
        $response = $this->success('Đăng nhập thành công', self::OK, Auth::user());
        $response['token'] = $token;
        return $response;
    }

    public function forgetPassword($email)
    {
        $user = $this->getUserByEmail($email)->first();
        if (!$user) {
            return $this->error('Email không tồn tại', self::BAD_REQUEST);
        }
        return $this->success('Hãy truy cập vào '.$email.' để xác thực email', self::OK);
    }

    public function show($userId)
    {
        $user = $this->getUserById($userId)->first();
        if (!$user) {
            return $this->error('User không tồn tại', self::NOT_FOUND);
        }
        return $this->success('Thành công', self::OK, $user);
    }

    public function changePassword($data)
    {
        $user = Auth::user();
        if (!Hash::check($data['old_password'], $user->password)) {
            return $this->error('Sai mật khẩu', self::BAD_REQUEST);
        }
        $update = $user->update(['password' => bcrypt($data['new_password'])]);
        if (!$update) {
            return $this->error('Cập nhật mật khẩu lỗi, xin thực hiện lại', self::BAD_REQUEST);
        }
        return $this->success('Cập nhật mật khẩu thành công', self::OK);
    }
}
