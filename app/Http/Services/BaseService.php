<?php
namespace App\Http\Services;

class BaseService {

    //const status code
    const
        OK = 200,
        CREATED = 201,
        CONTENT = 204,

        BAD_REQUEST = 400,
        UNAUTHORIZED = 401,
        FORBIDDEN = 403,
        NOT_FOUND = 404;



    public function error($message, $statusCode)
    {
        return [
            'success' => false,
            'message' => $message,
            'status_code' => $statusCode
        ];
    }

    public function success($message, $statusCode, $data = [])
    {
        return [
            'success' => true,
            'message' => $message,
            'status_code' => $statusCode,
            'data' => $data,
        ];
    }
}
