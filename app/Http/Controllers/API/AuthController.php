<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Services\UserService;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $registerRequest)
    {
        $data = $registerRequest->only('full_name', 'email', 'password', 'role');
        $response = $this->userService->store($data);
        return $this->response($response);
    }

    public function login(LoginRequest $loginRequest)
    {
        $credentials = $loginRequest->only('email', 'password');
        $response = $this->userService->login($credentials);
        return $this->response($response);
    }

    public function forgetPassword(ForgetPasswordRequest $forgetPasswordRequest)
    {
        $response = $this->userService->forgetPassword($forgetPasswordRequest->email);
        return $this->response($response);
    }
}
