<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Services\UserService;
use App\User;

class UserController extends Controller
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function show($userId)
    {
        $response = $this->userService->show($userId);
        return $this->response($response);
    }

    public function changePassword(ChangePasswordRequest $changePasswordRequest)
    {
        $data = $changePasswordRequest->only('old_password', 'new_password');
        $response = $this->userService->changePassword($data);
        return $this->response($response);
    }
}
