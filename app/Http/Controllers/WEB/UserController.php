<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function show($userId)
    {
        $users = $this->user->get();
        $ids = [];
        foreach ($users as $user) {
            $ids[] = $user->id;
        }
    }
}
