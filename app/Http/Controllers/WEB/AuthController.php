<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function actionRegister(RegisterRequest $registerRequest)
    {
        return redirect()->route('auth.login');
    }

    public function login()
    {
        return view('auth.login');
    }
}
