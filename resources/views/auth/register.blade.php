<form method="post" action="{{ route('auth.actionRegister') }}">
    @csrf
    <div class="form-group">
        <label>Email</label><br>
        <input type="email" name="email" placeholder="Email" class="form-control" value="{{ old('email') }}">
        <span>{{ $errors->first('email') }}</span>
    </div>
    <div class="form-group">
        <label>Password</label><br>
        <input type="password" name="password" placeholder="Password" class="form-control">
        <span>{{ $errors->first('password') }}</span>
    </div>
    <div class="form-group">
        <label>Role</label><br>
        <select class="form-control" name="role">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
    <div class="form-group">
        <button type="reset" class="btn btn-secondary">Reset</button>
        <button type="submit" class="btn btn-primary">Register</button>
    </div>
</form>
