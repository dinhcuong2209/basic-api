<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');

        DB::table('users')->insert([
            'full_name' => 'Admin',
            'email' => 'dinhcuong@ex.com',
            'role' => 1,
            'password' => Hash::make('123456'),
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        DB::table('users')->insert([
            'full_name' => 'Dinh Cuong',
            'email' => 'user@ex.com',
            'role' => 2,
            'password' => bcrypt('123456'),
        ]);
    }
}
